/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.debugger;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Set;
import org.netbeans.api.debugger.ActionsManager;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.jpda.JPDADebugger;
import org.netbeans.api.debugger.jpda.LineBreakpoint;
import org.netbeans.spi.debugger.ActionsProviderSupport;
import org.netbeans.spi.debugger.ContextProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class RToggleBreakpointActionProvider extends ActionsProviderSupport implements PropertyChangeListener {

	private JPDADebugger debugger;

	public RToggleBreakpointActionProvider() {
		//ModuleLib.log("RToggleBreakpointActionProvider()");
		Context.addPropertyChangeListener(this);
		setEnabled(ActionsManager.ACTION_TOGGLE_BREAKPOINT, false);
	}

	public RToggleBreakpointActionProvider(ContextProvider contextProvider) {
		ModuleLib.log("RToggleBreakpointActionProvider(ContextProvider contextProvider)");
		debugger = (JPDADebugger) contextProvider.lookupFirst(null, JPDADebugger.class);
		debugger.addPropertyChangeListener(JPDADebugger.PROP_STATE, this);
		Context.addPropertyChangeListener(this);
		setEnabled(ActionsManager.ACTION_TOGGLE_BREAKPOINT, false);
	}

	@Override
	public void doAction(Object o) {
		ModuleLib.log("RToggleBreakpointActionProvider::doAction() " + o);
		DebuggerManager debugManager = DebuggerManager.getDebuggerManager();

		// 1) get source name & line number
		int lineNumber = Context.getCurrentLineNumber();
		String url = Context.getCurrentURL();
		if (url == null) {
			return;
		}

		// 2) find and remove existing line breakpoint
		for (Breakpoint breakpoint : debugManager.getBreakpoints()) {
			if (breakpoint instanceof LineBreakpoint) {
				LineBreakpoint lineBreakpoint = ((LineBreakpoint) breakpoint);
				if (lineNumber == lineBreakpoint.getLineNumber() && url.equals(lineBreakpoint.getURL())) {
					debugManager.removeBreakpoint(breakpoint);
					return;
				}
			}
		}

		try {
			debugManager.addBreakpoint(RBreakpointFactory.create(url, lineNumber));
		} catch (MalformedURLException ex) {
			Exceptions.printStackTrace(ex);
		}

		for (Breakpoint bp : debugManager.getBreakpoints()) {
			if (bp instanceof LineBreakpoint) {
				ModuleLib.log("rbp=" + ((LineBreakpoint) bp).getLineNumber());
			}
		}
	}

	@Override
	public Set getActions() {
		return Collections.singleton(ActionsManager.ACTION_TOGGLE_BREAKPOINT);
	}

	private void destroy() {
		debugger.removePropertyChangeListener(JPDADebugger.PROP_STATE, this);
		Context.removePropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		ModuleLib.log("RToggleBreakpointActionProvider::propertyChange()");
		FileObject fo = Context.getCurrentFile();
		boolean isTinyFile = fo != null && "text/x-r".equals(fo.getMIMEType());

		setEnabled(ActionsManager.ACTION_TOGGLE_BREAKPOINT, isTinyFile);
		if (debugger != null && debugger.getState() == JPDADebugger.STATE_DISCONNECTED) {
			destroy();
		}
	}
}
