/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax;

import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.antlrgenerate.RLexer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class RLanguageHierarchy extends LanguageHierarchy<RTokenId> {

	private static List<RTokenId> tokens = new ArrayList<RTokenId>();
	private static Map<Integer, RTokenId> idToToken = new HashMap<Integer, RTokenId>();

	static {
		int x = 1;
		for (String ruleName : RLexer.ruleNames) {
			//ModuleLib.log("RLanguageHierarchy static, " + x + " = " + ruleName);
			RTokenId token = new RTokenId(ruleName, ruleName, x);
			idToToken.put(x, token);
			tokens.add(token);
			x++;
		}
	}

	public static synchronized RTokenId getToken(int id) {
//		ModuleLib.log("getToken()=" + id + ", len=" + idToToken.size());
		return idToToken.get(id);
	}

	@Override
	protected synchronized Collection<RTokenId> createTokenIds() {
//		ModuleLib.log("createTokenIds(), size=" + tokens.size());
		return tokens;
	}

	@Override
	protected synchronized Lexer<RTokenId> createLexer(LexerRestartInfo<RTokenId> info) {
//		ModuleLib.log("createLexer()");
		return new REditorLexer(info);
	}

	@Override
	protected String mimeType() {
		return "text/x-r";
	}
}
