# Netbeans-R
R development environment for netbeans.

## Pre-Compile

mvn install:install-file -Dfile=MainModule/libInUse/REngine.jar -DgroupId=org.rosuda -DartifactId=REngine -Dversion=1.8.5 -Dpackaging=jar

mvn install:install-file -Dfile=MainModule/libInUse/Rserve.jar -DgroupId=org.rosuda -DartifactId=Rserve -Dversion=1.8.5 -Dpackaging=jar

## Compile

Open project in netbeans, click Run
