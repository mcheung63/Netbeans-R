/*
 * Copyright (C) 2017 Peter (peter@quantr.hk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.debugger;

import java.net.MalformedURLException;
import java.net.URL;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.jpda.LineBreakpoint;
import org.netbeans.api.java.classpath.ClassPath;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;

public class RBreakpointFactory {

	static Breakpoint create(String url, int lineNumber) throws MalformedURLException {
		LineBreakpoint tinyBreakpoint = LineBreakpoint.create(url, lineNumber);
		FileObject file = URLMapper.findFileObject(new URL(url));
		tinyBreakpoint.setStratum("Tiny");
		tinyBreakpoint.setSourceName(file.getNameExt());
		tinyBreakpoint.setSourcePath(getGroovyPath(url));
		tinyBreakpoint.setPreferredClassName("anonymous");
		tinyBreakpoint.setPrintText(String.format("Breakpoint @%d", lineNumber));
		tinyBreakpoint.setHidden(false);

		return tinyBreakpoint;
	}

	private static String getGroovyPath(String url) throws MalformedURLException {
		FileObject fo = URLMapper.findFileObject(new URL(url));
		String relativePath = url;

		if (fo != null) {
			ClassPath cp = ClassPath.getClassPath(fo, ClassPath.SOURCE);
			if (cp == null) {
				return null;
			}
			FileObject root = cp.findOwnerRoot(fo);
			if (root == null) {
				return null;
			}
			relativePath = FileUtil.getRelativePath(root, fo);
		}

		return relativePath;
	}
}
