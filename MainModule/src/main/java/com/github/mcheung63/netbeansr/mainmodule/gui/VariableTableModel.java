package com.github.mcheung63.netbeansr.mainmodule.gui;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class VariableTableModel extends DefaultTableModel {

	String columnNames[] = {"Name", "Value"};
	Vector<String> names = new Vector<String>();
	Vector<Object> values = new Vector<Object>();

	public VariableTableModel() {
	}

	public String getColumnName(int column) {
		return columnNames[column];
	}

	public int getColumnCount() {
		if (columnNames == null) {
			return 0;
		}
		return columnNames.length;
	}

	public int getRowCount() {
		if (names == null) {
			return 0;
		}
		return names.size();
	}

	public void setValueAt(Object aValue, int row, int column) {
		this.fireTableDataChanged();
	}

	public Object getValueAt(final int row, int column) {
		try {
			if (column == 0) {
				return names.get(row);
			} else if (column == 1) {
				return values.get(row);
			} else {
				return "";
			}
		} catch (Exception ex) {
			return null;
		}
	}

	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex).getClass();
	}
}
