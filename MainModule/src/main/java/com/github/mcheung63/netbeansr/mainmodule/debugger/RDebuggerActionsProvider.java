/*
 * Copyright (C) 2017 Peter (peter@quantr.hk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.debugger;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.debugger.jpda.JPDADebugger;
import org.netbeans.spi.debugger.ActionsProviderSupport;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;

public class RDebuggerActionsProvider extends ActionsProviderSupport {

	public static final Object ACTION_STEP_OVER = "stepOver";
	public static final Object ACTION_STEP_BACK_OVER = "stepBackOver";
	public static final Object ACTION_RUN_INTO_METHOD = "runIntoMethod";
	public static final Object ACTION_STEP_INTO = "stepInto";
	public static final Object ACTION_STEP_BACK_INTO = "stepBackInto";
	public static final Object ACTION_STEP_OUT = "stepOut";
	public static final Object ACTION_STEP_OPERATION = "stepOperation";
	public static final Object ACTION_CONTINUE = "continue";
	public static final Object ACTION_REWIND = "rewind";
	public static final Object ACTION_START = "start";
	public static final Object ACTION_KILL = "kill";
	public static final Object ACTION_MAKE_CALLER_CURRENT = "makeCallerCurrent";
	public static final Object ACTION_MAKE_CALLEE_CURRENT = "makeCalleeCurrent";
	public static final Object ACTION_PAUSE = "pause";
	public static final Object ACTION_RUN_TO_CURSOR = "runToCursor";
	public static final Object ACTION_RUN_BACK_TO_CURSOR = "runBackToCursor";
	public static final Object ACTION_POP_TOPMOST_CALL = "popTopmostCall";
	public static final Object ACTION_FIX = "fix";
	public static final Object ACTION_RESTART = "restart";

	public static final String R_DEBUGGER_INFO = "RDebuggerInfo";
	public static final String R_SESSION = "RSession";

	private static final Set actions = new HashSet();

	static {
		ModuleLib.log("RDebuggerActionsProvider static");
		actions.add(ACTION_RUN_BACK_TO_CURSOR);
		actions.add(ACTION_STEP_BACK_INTO);
		actions.add(ACTION_STEP_BACK_OVER);
		actions.add(ACTION_REWIND);
		actions.add(ACTION_KILL);
		actions.add(ACTION_PAUSE);
		actions.add(ACTION_CONTINUE);
		actions.add(ACTION_START);
		actions.add(ACTION_STEP_INTO);
		actions.add(ACTION_STEP_OVER);
		actions.add(ACTION_RUN_TO_CURSOR);
	}

	public RDebuggerActionsProvider(ContextProvider contextProvider) {
		ModuleLib.log("RDebuggerActionsProvider");
		for (Iterator it = actions.iterator(); it.hasNext();) {
			setEnabled(it.next(), true);
		}
	}

	public static void startDebugger() {
		ModuleLib.log("startDebugger");
		try {
			Map<String, Object> services = new HashMap<>();
			services.put("name", "hi");
			services.put("baseDir", new File("/Users/peter/NetBeansProjects/Simple-R-Project"));
			//services.put("sourcepath", new File("/Users/peter/NetBeansProjects/Simple-R-Project/main.R"));

			JPDADebugger.attach("127.0.0.1", 8000, new Object[]{services});
		} catch (Exception ex) {
			ModuleLib.log("RProject ex=" + ex.getMessage());
		}
//		DebuggerManager manager = DebuggerManager.getDebuggerManager();
//		DebuggerInfo info = DebuggerInfo.create(R_DEBUGGER_INFO,
//				new Object[]{
//					new SessionProvider() {
//
//				@Override
//				public String getSessionName() {
//					return "R Program";
//				}
//
//				@Override
//				public String getLocationName() {
//					return "localhost";
//				}
//
//				public String getTypeID() {
//					return R_SESSION;
//				}
//
//				public Object[] getServices() {
//					return new Object[]{};
//				}
//			}, null
//				});
//
//		manager.startDebugging(info);
	}

	@Override
	public void doAction(Object action) {
		ModuleLib.log("RDebuggerActionsProvider::doAction " + action);
		if (action == ACTION_RUN_BACK_TO_CURSOR) {
		} else if (action == ACTION_STEP_BACK_INTO) {
		} else if (action == ACTION_STEP_BACK_OVER) {
		} else if (action == ACTION_REWIND) {
		} else if (action == ACTION_KILL) {
			//engineProvider.getDestructor().killEngine();
		} else if (action == ACTION_PAUSE) {
		} else if (action == ACTION_CONTINUE) {
		} else if (action == ACTION_START) {
		} else if (action == ACTION_STEP_INTO) {
		} else if (action == ACTION_STEP_OVER) {
		} else if (action == ACTION_RUN_TO_CURSOR) {
		}
	}

	@Override
	public Set getActions() {
		ModuleLib.log("getActions");
		return actions;
	}

}
