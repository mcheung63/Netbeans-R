///*
// * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
// *
// * This program is free software: you can redistribute it and/or modify
// * it under the terms of the GNU General Public License as published by
// * the Free Software Foundation, either version 3 of the License, or
// * (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//package com.github.mcheung63.netbeansr.mainmodule.filetype.r.parser;
//
//import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.antlrgenerate.RLexer;
//import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.antlrgenerate.RParser;
//import javax.swing.event.ChangeListener;
//import org.antlr.v4.runtime.CommonTokenStream;
//import org.antlr.v4.runtime.Lexer;
//import org.netbeans.modules.parsing.api.Snapshot;
//import org.netbeans.modules.parsing.api.Task;
//import org.netbeans.modules.parsing.spi.ParseException;
//import org.netbeans.modules.parsing.spi.Parser;
//import org.netbeans.modules.parsing.spi.SourceModificationEvent;
//
//public class NBParser extends Parser {
//
//    private Snapshot snapshot;
//    private RParser oracleParser;
//
//    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) {
//        this.snapshot = snapshot;
//        ANTLRStringStream input = new ANTLRStringStream(snapshot.getText().toString());
//        Lexer lexer = new RLexer(input);
//        CommonTokenStream tokens = new CommonTokenStream(lexer);
//        oracleParser = new RParser(tokens);
//        try {
//            oracleParser.prog();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public Result getResult(Task task) {
//        return new REditorParserResult(snapshot, oracleParser);
//    }
//
//    public void cancel() {}
//
//    public void addChangeListener(ChangeListener changeListener) {}
//
//    public void removeChangeListener(ChangeListener changeListener) {}
//
//    public static class REditorParserResult extends Result {
//
//        private RParser rParser;
//        private boolean valid = true;
//
//        REditorParserResult(Snapshot snapshot, RParser rParser) {
//            super(snapshot);
//            this.rParser = rParser;
//        }
//
//        public RParser getSqlParser()
//                throws ParseException {
//            if (!valid) {
//                throw new ParseException();
//            }
//            return rParser;
//        }
//
//        protected void invalidate() {
//            valid = false;
//        }
//    }
//}