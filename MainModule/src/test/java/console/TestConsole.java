/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.junit.Test;


public class TestConsole {

	@Test
	public void startSession() throws Exception {
		Process p = Runtime.getRuntime().exec("/opt/local/Library/Frameworks/R.framework/Versions/3.4/Resources/bin/R");
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		String s;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}

		while ((s = stdError.readLine()) != null) {
			System.out.println("error=" + s);
		}
	}
}
