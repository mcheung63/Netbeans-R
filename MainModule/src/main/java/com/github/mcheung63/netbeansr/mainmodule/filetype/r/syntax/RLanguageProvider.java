///*
// * Copyright (C) 2017 peter
// *
// * This program is free software: you can redistribute it and/or modify
// * it under the terms of the GNU General Public License as published by
// * the Free Software Foundation, either version 3 of the License, or
// * (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//package com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax;
//
//import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
//import org.netbeans.api.lexer.InputAttributes;
//import org.netbeans.api.lexer.Language;
//import org.netbeans.api.lexer.LanguagePath;
//import org.netbeans.api.lexer.Token;
//import org.netbeans.spi.lexer.LanguageEmbedding;
//import org.netbeans.spi.lexer.LanguageProvider;
//
//@org.openide.util.lookup.ServiceProvider(service = org.netbeans.spi.lexer.LanguageProvider.class)
//public class RLanguageProvider extends LanguageProvider {
//
//	@Override
//	public Language<?> findLanguage(String mimeType) {
//		ModuleLib.log("mimeType=" + mimeType);
//		if ("text/x-r".equals(mimeType)) {
//			return new RLanguageHierarchy().language();
//		}
//
//		return null;
//	}
//
//	@Override
//	public LanguageEmbedding<?> findLanguageEmbedding(Token<?> token, LanguagePath lp, InputAttributes ia) {
//		return null;
//	}
//
//}
