package antlr;

import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.RLanguageHierarchy;
import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.RTokenId;
import com.github.mcheung63.netbeansr.mainmodule.filetype.r.syntax.antlrgenerate.RLexer;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.junit.Test;

public class ANTLRDemo {

	@Test
	public void testRParser() throws Exception {
		System.out.println("testRParser");
		RLexer lexer = new RLexer(new ANTLRInputStream(getClass().getResourceAsStream("test_1.R")));
//		RParser parser = new RParser(new CommonTokenStream(lexer));
//		parser.addErrorListener(new BaseErrorListener() {
//			@Override
//			public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
//				throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
//			}
//		});
//		parser.prog();

//		CommonTokenStream tokens = new CommonTokenStream(lexer);
//		System.out.println("tokens.size()=" + tokens.size());
//		for (Object o : tokens.getTokens()) {
//			Token t = (Token) o;
//			System.out.println(t);
//		}
		org.antlr.v4.runtime.Token token;
		while ((token = lexer.nextToken()) != null) {
			if (token.getType() == RLexer.EOF) {
				break;
			}
			RTokenId tokenId = RLanguageHierarchy.getToken(token.getType());
			System.out.println("    > " + tokenId.name() + " = " + token.getText());
		}
	}
}
