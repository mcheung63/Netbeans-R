/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.projecttype;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import com.github.mcheung63.netbeansr.mainmodule.debugger.RDebuggerActionsProvider;
import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.debugger.jpda.JPDADebugger;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.ProjectState;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.DefaultProjectOperations;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class RProject implements Project {

	private final FileObject projectDir;
	private final ProjectState state;
	private Lookup lkp;

	RProject(FileObject dir, ProjectState state) {
		this.projectDir = dir;
		this.state = state;
	}

	@Override
	public FileObject getProjectDirectory() {
		return projectDir;
	}

	@Override
	public Lookup getLookup() {
		if (lkp == null) {
			lkp = Lookups.fixed(new Object[]{
				this,
				new RProjectInfo(),
				new RProjectLogicalView(this),
				new RCustomizerProvider(this),
				new RActionProvider()
			});
		}
		return lkp;
	}

	class RProjectInfo implements ProjectInformation {

		@StaticResource()
		public static final String PROJECT_ICON = "com/github/mcheung63/netbeansr/mainmodule/projecttype/logo.png";

		@Override
		public Icon getIcon() {
			// this icon is used in treeview of open project dialog
			return new ImageIcon(ImageUtilities.loadImage(PROJECT_ICON));
		}

		@Override
		public String getName() {
			return getProjectDirectory().getName();
		}

		@Override
		public String getDisplayName() {
			return getName();
		}

		@Override
		public void addPropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public void removePropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public Project getProject() {
			return RProject.this;
		}
	}

	class RProjectLogicalView implements LogicalViewProvider {

		@StaticResource()
		public static final String PROJECT_ICON = "com/github/mcheung63/netbeansr/mainmodule/projecttype/logo.png";

		private final RProject project;

		public RProjectLogicalView(RProject project) {
			this.project = project;
		}

		@Override
		public Node createLogicalView() {
			try {
				FileObject projectDirectory = project.getProjectDirectory();
				DataFolder projectFolder = DataFolder.findFolder(projectDirectory);
				Node nodeOfProjectFolder = projectFolder.getNodeDelegate();
				return new ProjectNode(nodeOfProjectFolder, project);
			} catch (DataObjectNotFoundException donfe) {
				Exceptions.printStackTrace(donfe);
				return new AbstractNode(Children.LEAF);
			}
		}

		private final class ProjectNode extends FilterNode {

			final RProject project;

			public ProjectNode(Node node, RProject project) throws DataObjectNotFoundException {
				super(node,
						NodeFactorySupport.createCompositeChildren(project, "Projects/org-r-project/Nodes"),
						new ProxyLookup(
								new Lookup[]{
									Lookups.singleton(project),
									node.getLookup()
								}));
				this.project = project;
			}

			@Override
			public Action[] getActions(boolean arg0) {
				return new Action[]{
					CommonProjectActions.newFileAction(),
					CommonProjectActions.copyProjectAction(),
					CommonProjectActions.deleteProjectAction(),
					CommonProjectActions.closeProjectAction(),
					CommonProjectActions.customizeProjectAction()
				};
			}

			@Override
			public Image getIcon(int type) {
				// this icon is used in treeview of projects window
				return ImageUtilities.loadImage(PROJECT_ICON);
			}

			@Override
			public Image getOpenedIcon(int type) {
				return getIcon(type);
			}

			@Override
			public String getDisplayName() {
				return project.getProjectDirectory().getName();
			}

		}

		@Override
		public Node findPath(Node root, Object target) {
			//leave unimplemented for now
			return null;
		}

	}

	class RActionProvider implements ActionProvider {

		private String[] supported = new String[]{
			ActionProvider.COMMAND_RUN,
			ActionProvider.COMMAND_DELETE,
			ActionProvider.COMMAND_COPY,
			ActionProvider.COMMAND_RUN,
			ActionProvider.COMMAND_DEBUG
		};

		@Override
		public String[] getSupportedActions() {
			return supported;
		}

		@Override
		public void invokeAction(String string, Lookup lookup) throws IllegalArgumentException {
			if (string.equalsIgnoreCase(ActionProvider.COMMAND_DELETE)) {
				DefaultProjectOperations.performDefaultDeleteOperation(RProject.this);
			} else if (string.equalsIgnoreCase(ActionProvider.COMMAND_RUN)) {
				ModuleLib.log("ActionProvider.COMMAND_RUN");
			} else if (string.equalsIgnoreCase(ActionProvider.COMMAND_DEBUG)) {
				RDebuggerActionsProvider.startDebugger();
			}
		}

		@Override
		public boolean isActionEnabled(String command, Lookup lookup) throws IllegalArgumentException {
			if ((command.equals(ActionProvider.COMMAND_DELETE))) {
				return true;
			} else if ((command.equals(ActionProvider.COMMAND_COPY))) {
				return true;
			} else if ((command.equals(ActionProvider.COMMAND_RUN))) {
				return true;
			} else if ((command.equals(ActionProvider.COMMAND_DEBUG))) {
				return true;
			} else {
				throw new IllegalArgumentException(command);
			}
		}
	}

}
