/*
 * Copyright (C) 2017 Peter (peter@quantr.hk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.projecttype;

import org.netbeans.api.debugger.DebuggerManagerAdapter;
import org.netbeans.api.debugger.Session;

public class SessionListener extends DebuggerManagerAdapter {

	@Override
	public void sessionAdded(Session session) {
		super.sessionAdded(session);
	}

	@Override
	public void sessionRemoved(Session session) {
		super.sessionRemoved(session);
	}
}
