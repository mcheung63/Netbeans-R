/*
 * Copyright (C) 2017 peter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.gui;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(
		category = "R",
		id = "com.github.mcheung63.netbeansr.mainmodule.gui.TestAction"
)
@ActionRegistration(
		iconBase = "com/github/mcheung63/netbeansr/mainmodule/gui/tick.png",
		displayName = "#CTL_TestAction"
)
@ActionReference(path = "Toolbars/R", position = -99)
@Messages("CTL_TestAction=Test")
public final class TestAction implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		TopComponent active = TopComponent.getRegistry().getActivated();
		ModuleLib.log(active);
	}
}
