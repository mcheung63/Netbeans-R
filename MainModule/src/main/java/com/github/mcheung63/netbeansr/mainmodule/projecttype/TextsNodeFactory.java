/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.projecttype;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

@NodeFactory.Registration(projectType = "org-r-project", position = 10)
public class TextsNodeFactory implements NodeFactory {

	@Override
	public NodeList<?> createNodes(Project project) {
		RProject p = project.getLookup().lookup(RProject.class);
		assert p != null;
		return new TextsNodeList(p);
	}

	private class TextsNodeList implements NodeList<Node> {

		RProject project;

		public TextsNodeList(RProject project) {
			this.project = project;
		}

		@Override
		public List<Node> keys() {
			FileObject projectFolder = project.getProjectDirectory();
			List<Node> result = new ArrayList<Node>();
			if (projectFolder != null) {
				for (FileObject textsFolderFile : projectFolder.getChildren()) {
					try {
						if (!textsFolderFile.getName().toLowerCase().equals(RProjectFactory.PROJECT_FILE)) {
							result.add(DataObject.find(textsFolderFile).getNodeDelegate());
						}
					} catch (DataObjectNotFoundException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}
			return result;
		}

		@Override
		public Node node(Node node) {
			return new FilterNode(node);
		}

		@Override
		public void addNotify() {
		}

		@Override
		public void removeNotify() {
		}

		@Override
		public void addChangeListener(ChangeListener cl) {
		}

		@Override
		public void removeChangeListener(ChangeListener cl) {
		}

	}

}
