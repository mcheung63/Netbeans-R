/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.projecttype;

import javax.swing.JComponent;
import javax.swing.JPanel;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.netbeans.spi.project.ui.support.ProjectCustomizer.Category;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class GeneralCustomerProperties implements ProjectCustomizer.CompositeCategoryProvider {

	private static final String GENERAL = "General";

	@ProjectCustomizer.CompositeCategoryProvider.Registration(projectType = "org-r-project", position = 10)
	public static GeneralCustomerProperties createGeneral() {
		return new GeneralCustomerProperties();
	}

	@NbBundle.Messages("LBL_Config_General=General")
	@Override
	public Category createCategory(Lookup lkp) {
		return ProjectCustomizer.Category.create(
				GENERAL,
				Bundle.LBL_Config_General(),
				null);
	}

	@Override
	public JComponent createComponent(Category category, Lookup lkp) {
		return new JPanel();
	}

}
