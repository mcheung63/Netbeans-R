/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.debugger;

import org.netbeans.api.debugger.DebuggerEngine;
import org.netbeans.api.debugger.Session;
import org.netbeans.api.debugger.jpda.JPDADebugger;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.debugger.DebuggerEngineProvider;

public class RDebuggerEngineProvider extends DebuggerEngineProvider{

	private DebuggerEngine.Destructor desctuctor;
	private Session session;

	public RDebuggerEngineProvider(ContextProvider contextProvider) {
		session = (Session) contextProvider.lookupFirst(null, Session.class);
	}

	public String[] getLanguages() {
		return new String[]{"R"};
	}

	public String getEngineTypeID() {
		return JPDADebugger.ENGINE_ID;
	}

	public Object[] getServices() {
		return new Object[0];
	}

	public void setDestructor(DebuggerEngine.Destructor desctuctor) {
		this.desctuctor = desctuctor;
	}

	public DebuggerEngine.Destructor getDestructor() {
		return desctuctor;
	}

	public Session getSession() {
		return session;
	}
}
