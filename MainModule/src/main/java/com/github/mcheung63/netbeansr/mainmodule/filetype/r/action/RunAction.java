/*
 * Copyright (C) 2017 Peter (mcheung63@hotmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule.filetype.r.action;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import com.github.mcheung63.netbeansr.mainmodule.filetype.r.RDataObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "File",
		id = "com.github.mcheung63.netbeansr.mainmodule.filetype.r.action.RunAction"
)
@ActionRegistration(
		iconBase = "com/github/mcheung63/netbeansr/mainmodule/filetype/r/action/resultset_next.png",
		displayName = "#CTL_RunAction"
)
@ActionReference(path = "Loaders/text/x-r/Actions", position = 150)
@Messages("CTL_RunAction=Run")
public final class RunAction implements ActionListener {

	private final RDataObject context;

	public RunAction(RDataObject context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		ModuleLib.log("RunAction::actionPerformed()");
		FileObject f = context.getPrimaryFile();
		String displayName = FileUtil.getFileDisplayName(f);
		String msg = "I am " + displayName + ". Hear me roar!";
		NotifyDescriptor nd = new NotifyDescriptor.Message(msg);
		DialogDisplayer.getDefault().notify(nd);
	}
}
