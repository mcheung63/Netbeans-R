/*
 * Copyright (C) 2017 Peter (peter@quantr.hk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.mcheung63.netbeansr.mainmodule;

import com.github.mcheung63.netbeansr.mainmodule.gui.RConsoleWindowTopComponent;

public class Helper {

	public static RConsoleWindowTopComponent currentConsoleWindow;

	public static String[] setCommand(String command) {
		currentConsoleWindow.writer.write("library(Rserve)\n");
		currentConsoleWindow.writer.write("run.Rserve()\n");
		currentConsoleWindow.writer.flush();
		
		return null;
	}

}
