package com.github.mcheung63.netbeansr.mainmodule.rserve;

import com.github.mcheung63.netbeansr.mainmodule.ModuleLib;
import java.io.*;
import javax.swing.JEditorPane;
import org.openide.util.Exceptions;
import org.rosuda.REngine.Rserve.RConnection;

public class Rserve {

	PrintWriter writer;
	JEditorPane editorPane;

	public boolean checkAndRunRserve(PrintWriter writer, JEditorPane editorPane) {
		this.writer = writer;
		this.editorPane = editorPane;
		if (isRserveRunning()) {
			return true;
		}
		String osname = System.getProperty("os.name");
		if (osname != null && osname.length() >= 7 && osname.substring(0, 7).equals("Windows")) {
			ModuleLib.log("Windows: query registry to find where R is installed ...");
			String installPath = null;
			try {
				Process rp = Runtime.getRuntime().exec("reg query HKLM\\Software\\R-core\\R");
				rp.waitFor();
			} catch (Exception rge) {
				ModuleLib.log("ERROR: unable to run REG to find the location of R: " + rge);
				return false;
			}
			if (installPath == null) {
				ModuleLib.log("ERROR: canot find path to R. Make sure reg is available and R was installed with registry settings.");
				return false;
			}
			return launchRserve(installPath + "\\bin\\R.exe");
		} else {
			return launchRserve("/opt/local/Library/Frameworks/R.framework/Versions/3.4/Resources/bin/R");
		}
	}

	public static boolean isRserveRunning() {
		try {
			RConnection c = new RConnection();
			ModuleLib.log("Rserve is running.");
			c.close();
			return true;
		} catch (Exception e) {
			ModuleLib.log("First connect try failed with: " + e.getMessage());
		}
		return false;
	}

	public boolean launchRserve(String cmd) {
		return launchRserve(cmd, "--no-save --slave", "--no-save --slave", false);
	}

	public boolean launchRserve(String cmd, String rargs, String rsrvargs, boolean debug) {
		try {
			ModuleLib.log("launchRserve");
			Process p;
			boolean isWindows = false;
			String osname = System.getProperty("os.name");
			if (osname != null && osname.length() >= 7 && osname.substring(0, 7).equals("Windows")) {
				isWindows = true;
				/* Windows startup */
				p = Runtime.getRuntime().exec("\"" + cmd + "\" -e \"library(Rserve);Rserve(" + (debug ? "TRUE" : "FALSE") + ",args='" + rsrvargs + "')\" " + rargs);
			} else /* unix startup */ {
				ModuleLib.log("launchRserve, " + "echo 'library(Rserve);Rserve(" + (debug ? "TRUE" : "FALSE") + ",args=\"" + rsrvargs + "\")'|" + cmd + " " + rargs);
				p = Runtime.getRuntime().exec(new String[]{
					"/bin/sh", "-c",
					"echo 'library(Rserve);Rserve(" + (debug ? "TRUE" : "FALSE") + ",args=\"" + rsrvargs + "\")'|" + cmd + " " + rargs
				});
//				p = Runtime.getRuntime().exec("/Users/peter/Library/R/3.4/library/Rserve/libs//Rserve --no-save --slave");
			}
			ModuleLib.log("waiting for Rserve to start ... (" + p + ")");
			// we need to fetch the output - some platforms will die if you don't ...

			final InputStreamReader stdInput = new InputStreamReader(p.getInputStream());
			final BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			writer = new PrintWriter(p.getOutputStream());
			ModuleLib.log("writer=" + writer);

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						int s;
						while ((s = stdInput.read()) != -1) {
							//							ModuleLib.log(">>" + s + "<<" + (char) s + "<<");
							if (s == 8) {
								editorPane.setText(editorPane.getText().substring(0, editorPane.getText().length() - 1));
							} else {
								editorPane.setText(editorPane.getText() + (char) s);
							}

							editorPane.setCaretPosition(editorPane.getText().length());
						}
					} catch (IOException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}).start();

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String s;
						while ((s = stdError.readLine()) != null) {
							editorPane.setText(editorPane.getText() + s + "\n");
							editorPane.setCaretPosition(editorPane.getText().length());
						}
					} catch (IOException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}).start();
			int attempts = 5;
			while (attempts > 0) {
				try {
					RConnection c = new RConnection();
					ModuleLib.log("Rserve is running.");
					c.close();
					return true;
				} catch (Exception e2) {
					ModuleLib.log("Try failed with: " + e2.getMessage());
				}
				/* a safety sleep just in case the start up is delayed or asynchronous */
				try {
					Thread.sleep(500);
				} catch (InterruptedException ix) {
				};
				attempts--;
			}

			return true;
		} catch (Exception ex) {
			return false;
		}
	}

}
